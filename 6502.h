#pragma once

#include <vector>
#include <string>
#include <map>
#include <stdio.h>

class Bus;

class _6502
{
public:
        _6502();
        ~_6502();
public:
        uint8_t  a      = 0x00;

private:
        uint8_t GetFlag(FLAGS6502 f);
        void    SetFlag(FLAGS6502 f, bool v);

        uint8_t  fetched        = 0x00;
        uint16_t temp           = 0x0000;
        uint16_t addr_abs       = 0x0000;
        uint16_t addr_rel       = 0x0000;
        uint8_t  opcode         = 0x00;
        uint8_t  cycles         = 0;
        uint32_t clock_count    = 0;

        uint8_t read(uint16_t a);
        void    write(uint16_t a, uint8_t d);

        uint8_t fetch();
private:

        uint8_t IMP(); uint8_t IMM();
        uint8_t ZP0(); uint8_t ZPX();
        uint8_t ZPY(); uint8_t REL();
        uint8_t ABS(); uint8_t ABX();
        uint8_t ABY(); uint8_t IND();
        uint8_t IZX(); uint8_t IZY();
};
